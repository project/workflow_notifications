CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers


INTRODUCTION
------------

This module provides a way to configure a mail for state transition by the
support of workflow module. Based on the state change, Mail template can
configure. This is applicable for all entities. By using this, can also trigger
a mail based on role or particular mailId.


REQUIREMENTS
------------

 * Workflow (https://www.drupal.org/project/workflow)


INSTALLATION
------------

Install the module as you would normally install a contributed Drupal module.

  Visit:  https://www.drupal.org/node/1897420 for directions to installing.

  Visit: https://www.drupal.org/project/workflow_notifications/git-instructions
  for cloning the project repository.


CONFIGURATION
-------------

Go to Administration >> Configuration >> Workflow to configure the module.


TROUBLESHOOTING
---------------

If the module is not shown in the list, try deleting the module and try cloning
it again. Or else try clearing the cache, and then try installing it.


MAINTAINERS
-----------

 * George Anderson - https://www.drupal.org/u/geoanders

 * Saranya Purushothaman - https://www.drupal.org/u/saranya-purushothaman

 * John Voskuilen - https://www.drupal.org/u/johnv
